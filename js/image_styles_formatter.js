(function($) {
  Drupal.behaviors.imageDownloadFormatter = {
    attach: function(context, settings) {
      $('.image-styles-formatter-wrapper', context).once('idf', function() {
        var $this = $(this),
        reload_image = $this.hasClass('reload-image'),

        changeImage = function(url) {
          if (reload_image) {
            if ($this.find('.image-style-wrapper').length > 0) {
              $this.find('.image-style-wrapper').find('img')
                .removeAttr('width')
                .removeAttr('height')
                .attr('src', url);
            }
            else {
              window.location = url;
            }
          }
          else {
            var win = window.open(url, '_blank');
            win.focus();
          }
        };

        if ($this.find('select').length > 0) {
          if ($this.find('.action-wrapper').length > 0) {
            $this.find('.action-wrapper').bind('click tap', function() {
              changeImage($this.find('select').val());
            });
          }
          else {
            $this.find('select').on('change', function() {
              changeImage($(this).val());
            });
          }
        }

        else {
          $this.find('.items-list-wrapper').find('li > a').bind('click tap', function(e) {
            e.preventDefault();
            changeImage($(this).attr('href'));
          });
        }
      });
    }
  };
})(jQuery);

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Usage


INTRODUCTION
------------
This small module allows you to output an imagefield in multiple image styles 
with different formats. Currently, the following formats are supported:

- List of links to images
- List of thumbnail images
- A select box of items, where each item's key is the link to the image and 
lable is a configurable text such as name of the image style, size and dimension
of the image file.


USAGE
------------
Install the module normally like other standard Drupal modules. This module 
depends on field & image modules which are already available from Drupal core.
In order to use the tokens for the style label, you need to enable token module
as well.

After installation:

1. Add one imagefield to any content type you want
2. Go to the "Manage Display" tab of the content type in question
3. Select the image field you have just created
4. Set the Formatter to "Image Styles"
5. Click on the Settings icon for the field
6. There you can choose what image styles to be included as well as how the 
result will be displayed.
